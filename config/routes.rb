Rails.application.routes.draw do

  namespace :api do
    get 'users', action: :getUsers, controller: :users
    post 'user', action: :addUser, controller: :users
    get 'user/:id', action: :showUser, controller: :users
    get 'typeahead/:input', action: :searchUser, controller: :users
    put 'user/:id', action: :updateUser, controller: :users
    delete 'user/:id', action: :deleteUser, controller: :users
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

class Api::UsersController < ApplicationController
    before_action :getUser, only: [:updateUser, :showUser, :deleteUser]
    def getUsers
      user = User.all
       if user
        render json: user, status: :ok
       else
        render json: { msg: "user empty"}, status: :unprocessible_entity
       end
    end
    def addUser
     user = User.new(userparams)
      if user.save
       render json: user, status: :ok
      else
       render json: { msg: "user not added"}, status: :unprocessible_entity
      end
    end

    def showUser
      if @user
        render json: @user, status: :ok
      else
       render json: { msg: "user not found"}, status: :unprocessible_entity
      end
    end

    def updateUser
      if @user
         if @user.update(userparams)
           render json: @user, status: :ok
         else
           render json: { msg: "updation failed"}, status: :unprocessible_entity
         end
      else
        render json: {msg: "user not found"}, status: :unprocessible_entity
      end
    end

    def deleteUser
      if @user
        if @user.destroy
          render json: {msg: "user deleted"}, status: :ok
        else
          render json: { msg: "deletion failed"}, status: :unprocessible_entity
        end
      else
       render json: {msg: "user not found"}, status: :unprocessible_entity
      end
    end

  
    def searchUser
      @parameter = params[:input]
      @results = User.or({firstName: /#{@parameter}/i}, {lastName: /#{@parameter}/i}, {email: /#{@parameter}/i})
      render json: @results, status: :ok 
end

    private
    def userparams
       params.permit(:firstName, :lastName, :email)
    end
    def getUser
       @user = User.find(params[:id])
    end   
 
end
